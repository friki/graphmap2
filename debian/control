Source: graphmap2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/graphmap2
Vcs-Git: https://salsa.debian.org/med-team/graphmap2.git
Homepage: https://github.com/lbcb-sci/graphmap2

Package: graphmap2
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: highly sensitive and accurate mapper for long, error-prone reads
 GraphMap2 is a highly sensitive and accurate mapper for long, error-
 prone reads. The mapping algorithm is designed to analyse nanopore
 sequencing reads, which progressively refines candidate alignments to
 robustly handle potentially high-error rates and a fast graph traversal
 to align long reads with speed and high precision (>95%). Evaluation on
 MinION sequencing data sets against short- and long-read mappers
 indicates that GraphMap increases mapping sensitivity by 10–80% and maps
 >95% of bases. GraphMap alignments enabled single-nucleotide variant
 calling on the human genome with increased sensitivity (15%) over the
 next best mapper, precise detection of structural variants from length
 100 bp to 4 kbp, and species and strain-specific identification of
 pathogens using MinION reads.
